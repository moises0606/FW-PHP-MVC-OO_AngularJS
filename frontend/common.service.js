eventplanner.factory("CommonService", ['$rootScope','$timeout', function ($rootScope, $timeout) {
    var service = {};
    service.banner = banner;
    service.amigable = amigable;
    service.toastrm = toastrm;
    service.eliminarDiacriticos = eliminarDiacriticos;

    return service;

    function banner(message, type) {
        $rootScope.bannerText = message;
        $rootScope.bannerClass = 'alertbanner aletbanner' + type;
        $rootScope.bannerV = true;

        $timeout(function () {
            $rootScope.bannerV = false;
            $rootScope.bannerText = "";
        }, 5000);
    }
    
    function amigable(url) {
        var link = "";
        url = url.replace("?", "");
        url = url.split("&");

        for (var i = 0; i < url.length; i++) {
            var aux = url[i].split("=");
            link += aux[1] + "/";
        }
        return link;
    }

    function toastrm(message, type, time) {
        switch(type) {
            case 'success':
                    toastr.success(message);
                break;
            case 'error':
                    toastr.error(message);
                break;
            case 'warning':
                    toastr.warning(message);
                break;
            case 'info':
                    toastr.info(message);
                break;
        }
    }

    function eliminarDiacriticos(texto) {
        return texto.normalize('NFD').replace(/[\u0300-\u036f]/g,"");
    }

}]);
