var eventplanner = angular.module('eventplanner',['ngRoute', 'ui.bootstrap'/*, 'ngAnimate', 'ngCookies', 'facebook'*/]);
eventplanner.config(['$routeProvider',
    function ($routeProvider) {
        $routeProvider
                
                // Contact
                .when("/contact", {
                    templateUrl: "frontend/modules/contact/view/contact.view.html", 
                    controller: "contactCtrl"
                })

                // Home
                .when("/home", {
                    templateUrl: "frontend/modules/home/view/home.view.html",
                    controller: "homeCtrl",
                    resolve: {
                        provinces: function (services) {
                            return services.get('home', 'category');
                        },
                        autocomplete: function (services) { /*Trae todas las entradas y categorias (se ha hecho un merge)*/
                            return services.get('home', 'entry');
                        }
                    }
                })
                // Shop (home_list)
                .when("/home_list", {
                    templateUrl: "frontend/modules/home/view/home_list.view.html",
                    controller: "shopCtrl",
                    resolve: {
                        events: function (services) {
                            return services.get('home', 'list1');
                        },
                        comments: function (services) {
                            return services.get('home', 'obtain_comentarios');
                        },
                        ratings: function (services) {
                            return services.get('home', 'obtain_rating');
                        }
                    }
                })
                //Shop (home_list)
                .when("/home_list/:cat", {
                    templateUrl: "frontend/modules/home/view/home_list.view.html",
                    controller: "shopCtrl",
                    resolve: {
                        events: function (services, $route) {
                            return services.get('home', 'list_results', $route.current.params.cat);
                        },
                        comments: function (services) {
                            return services.get('home', 'obtain_comentarios');
                        },
                        ratings: function (services) {
                            return services.get('home', 'obtain_rating');
                        }
                    }
                })
                //Login
                .when("/login", {
                    templateUrl: "frontend/modules/login/view/login.view.html", 
                    controller: "loginCtrl"
                })
                //Register
                .when("/register", {
                    templateUrl: "frontend/modules/login/view/register.view.html", 
                    controller: "registerCtrl"
                })
                //Register (activate)
                .when("/register/activate/:token", {
                    templateUrl: "frontend/modules/login/view/activate.view.html", 
                    controller: "verifyCtrl",
                    resolve: {
                        event: function (services, $route) {
                            return services.get('login', 'activate', $route.current.params.token);
                        }, 
                        token: function ($route) {
                            return $route.current.params.token;
                        }
                    }
                })
                //Recover
                .when("/recover", {
                    templateUrl: "frontend/modules/login/view/recover.view.html", 
                    controller: "recoverCtrl"
                })
                //Recover from email
                .when("/recover_pass/activate/:token", {
                    templateUrl: "frontend/modules/login/view/recover_pass.view.html", 
                    controller: "recover_passCtrl",
                    resolve: {
                        token: function (services, $route) {
                            return $route.current.params.token;
                        }
                    }
                })

                //Profile
                .when("/profile", {
                    templateUrl: "frontend/modules/login/view/profile.view.html", 
                    controller: "profileCtrl",
                    resolve: {
                        user: function (services, $route, localStorage) {
                                    return localStorage.get();
                        },
                        ratings: function (services) {
                            return services.get('login', 'obtain_rating');
                        },
                        events: function (services) {
                            return services.get('home', 'list1');
                        }
                    }
                })

                // else 404
                .otherwise("/home", {
                    templateUrl: "frontend/modules/home/view/home.view.html",
                    controller: "homeCtrl",
                    resolve: {
                        events: function (services) {
                            return services.get('home', 'category');
                        }
                    }
                });
    }]);
/*
eventplanner.config([
    'FacebookProvider',
    function (FacebookProvider) {
        var myAppId = '1651372404958355';
        FacebookProvider.init(myAppId);
    }
]);*/
