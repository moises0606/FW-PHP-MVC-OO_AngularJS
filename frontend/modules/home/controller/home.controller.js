eventplanner.controller('homeCtrl', function ($scope, provinces, autocomplete, services, CommonService) {
    $scope.showcat = 4;
    $scope.provincesv1 = provinces;
    $scope.provinces = [];
    loadMoreArray();

    //console.log($scope.provinces);
    var entrada=[];
    $.each(autocomplete, function (i, valor) {
        if (valor.prodname){
            entrada.push(valor.prodname);
        }
        if (valor.slug){
            entrada.push(valor.slug);
        }
        //console.log(entrada);
    });
        //console.log(entrada);
    $scope.entrada = entrada;

    $scope.complete = function(string){
        var output = [];
        angular.forEach($scope.entrada, function(key){
            if (key.toLowerCase().indexOf(string.toLowerCase()) >= 0){
                output.push(key);
            }
        });
        $scope.filterEntrada = output;
    }

    $scope.fillTextbox = function(string){
        $scope.key = string;
        $scope.hidethis = true;
    }

    $scope.search = function(){
        data = {clave:$scope.key};

        services.post('home', 'savecat', data).then(function (response) {
            //console.log(response);
            var json = JSON.parse(response);
            //console.log(json);
            
            if (json == 'true') {
                CommonService.toastrm("Redirigiendo", "info");
                setTimeout(function() { 
                    $callback = "#/home_list/"+$scope.key;
                    window.location.href=$callback;
                }, 500);
            } else {
                CommonService.toastrm("No se ha podido realizar la operacion", "error");            }
        });
    }


    $scope.loadMore = function() {
      $scope.showcat += 4;
      loadMoreArray();
    };
    
    $scope.hcatclick = function(slug) {
        //console.log(slug);
        data = {clave:slug};

        services.post('home', 'savecat', data).then(function (response) {
            //console.log(response);
            var json = JSON.parse(response);
            //console.log(json);
            
            if (json == 'true') {
                CommonService.toastrm("Redirigiendo", "info");
                setTimeout(function() { 
                    $callback = "#/home_list/"+slug;
                    window.location.href=$callback;
                }, 500);
            } else {
                CommonService.toastrm("No se ha podido realizar la operacion", "error");            }
        });
    };

    function loadMoreArray() {
    $scope.provinces = [];
      for (var i = 0; i < $scope.showcat; i++) {
          $scope.provinces.push($scope.provincesv1[i]);
      }
      //console.log($scope.provinces);
    }
});