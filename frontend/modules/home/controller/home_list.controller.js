eventplanner.controller('shopCtrl', function ($scope, events, services, CommonService, maps, comments, localStorage, ratings) {
    $scope.events = events;
    $scope.eventsprinted = [];
    $scope.eventsprinted = $scope.events.slice(0, 4);
	showMap();

    //console.log($scope.events);
    $scope.hlbody = true;
    //console.log(comments);

    $scope.hcatclick = function(slug) {
        $scope.hrating = [];
        $scope.eventcomment = [];
		$scope.hlbody = false;
		$scope.mapholderV = false;
		$scope.detail = true;
		$scope.event = slug;

        //console.log(slug);

        angular.forEach(comments, function(comment){
            if (comment.prodref == slug.prodref){
                $scope.eventcomment.push(comment);
            }
        });

        angular.forEach(ratings, function(rating){
            if (rating.entrada == slug.prodref){
                $scope.hrating.push(rating);
            }
        });
    };

/*
    $scope.hlike = function(slug) {

    };*/


    $scope.estrellas = function(num) {
        //console.log(num);
        localStorage.get().then(function (user){
            if (user){
                if (user.activado == 0){
                    CommonService.toastrm("Usuario desactivado", "error");
                }
                
                prodref = $scope.event.prodref;
                var puntuacion = {'prodref': prodref, 'user': user.uid, 'puntuacion':num};
                console.log(puntuacion);
                
                services.post("home", "insert_rating", puntuacion).then(function (data) {
                    console.log(data);
                });

                CommonService.toastrm("Puntuacion enviada", "success");

            }else{
                CommonService.toastrm("Debes estar registrado para escribir comentarios", "error");
            }
        });
    };

    $scope.newcoment = function() {
        //console.log($scope.sendnewcoment);
        comentario = [];
        if ($scope.sendnewcoment) {
            localStorage.get().then(function (user){
                //console.log(user);
                if (user){
                    if (user.activado == 0){
                        CommonService.toastrm("Usuario desactivado", "error");
                    }
                
                    comentario.prodref = $scope.event.prodref;
                    comentario.comment = user.usuario + ": " + $scope.sendnewcoment;
                    
                    console.log(comentario);
                    
                    prodref = $scope.event.prodref;
                    mensaje = $scope.sendnewcoment;
                    var datos = {'prodref': prodref, 'comment': user.usuario +": " + mensaje};
                    console.log(datos);
                    
                    services.post("home", "insert_comment", datos).then(function (data) {
                        console.log(data);
                        
                    });

                    $scope.eventcomment.push(comentario);
                    CommonService.toastrm("Comentario enviado", "success");

                }else{
                    CommonService.toastrm("Debes estar registrado para escribir comentarios", "error");
                }
            });
        }else
            CommonService.toastrm("Debes escribir el comentario", "error");

    }

    $scope.back = function() {
    	$scope.hlbody = true;
		$scope.mapholderV = true;
		$scope.detail = false;
    }

    function showMap () {
    	    $scope.mapholderV = true;
    	    //console.log($scope.eventsprinted);
            maps.cargarmap($scope.eventsprinted);
    };

	$scope.changePage = function() {
	  var startPos = ($scope.currentPage - 1) * 4;
	  $scope.eventsprinted = $scope.events.slice(startPos, startPos + 4);
      maps.cargarmap($scope.eventsprinted);
	  //console.log($scope.currentPage);
	};

});