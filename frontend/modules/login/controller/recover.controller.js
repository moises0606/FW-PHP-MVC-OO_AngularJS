eventplanner.controller('recoverCtrl', function ($scope, services, CommonService) {

	$scope.recover = function () {
        //console.log($scope);
        //console.log($scope.fregister.usuario);
        
        email = $scope.frecover.email;
        
        if (email) {
			var user = {"email": email};            
			console.log(user);

            services.post('login', 'recover_email', user).then(function (response) {
                console.log(response);
                $scope.error_email = '';

                if (response.error.email){
                    $scope.AlertMessage = true;
                    $scope.error_email = response.error.email;
                }else{
                    CommonService.toastrm("Revisa el correo", "success");
                    window.location.href='#/login';
                }
            });
        }
    };

});