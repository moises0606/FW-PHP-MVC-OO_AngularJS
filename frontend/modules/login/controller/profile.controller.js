eventplanner.controller('profileCtrl', function ($scope, services, CommonService, com_prov_city, user, LoginService, ratings, events) {
    //console.log(user);

    $scope.user = user;
    $scope.profile = user;
    $scope.avatar = true;
    $scope.button = true;
    $scope.profiledivform = false;

    $scope.perfil = function () {
        $scope.profiledivform = true;
        $scope.button = false;
    };

    $scope.likes = function () {
        
        $scope.button = false;
    };

    $scope.rating = function () {
        $scope.ratingdiv = true;
        $scope.button = false;
        $scope.ratings = [];

        angular.forEach(ratings, function(rating){
            if (user.uid == rating.user){
                angular.forEach(events, function(event){
                    if (event.prodref == rating.entrada){
                        rating.prodname = event.prodname;
                        rating.province = event.province;
                        rating.city = event.city;
                        rating.prodprice = event.prodprice;
                        $scope.ratings.push(rating);
                    }
                });
            }
        });
    };

    $scope.volver = function () {
        $scope.profiledivform = false;
        $scope.ratingdiv = false;
        $scope.button = true;
    };

    com_prov_city.load_com().then(function (response) {
        //console.log(response);
        
        if(response.success)
            $scope.comunidades = response.datas;
    });


    $scope.reset_comunidades = function () {
        //console.log($scope.profile.comunidad.slug);
        var comunidad = $scope.profile.comunidad.slug;

        com_prov_city.loadProvincia(comunidad).then(function (response) {
            //console.log(response);
            
            if(response.success)
                $scope.provincias = response.datas;
        });
    };

    $scope.reset_provincia = function () {
        //console.log($scope.profile.comunidad.slug);
        var provincia = $scope.profile.provincia.slug;
        //console.log(provincia);
        com_prov_city.loadCity(provincia).then(function (response) {
            console.log(response);
            
            if(response.success)
                $scope.city = response.datas.cities;
        });
    };

    $scope.submit = function () {

        console.log($scope.profile);

        if (!$scope.profile.comunidad) {
            com = " ";
        }else{
            com = $scope.profile.comunidad.slug;
        }

        if (!$scope.profile.provincia) {
            prov = " ";
        }else{ 
            prov = $scope.profile.provincia.slug;
        }

        if (!$scope.profile.city) { 
            pob = " ";
        }else{
            pob = $scope.profile.city.slug;
        }

        if (!$scope.profile.nombre) {
            nombre = "default"; //DNI GENERADO
        }else{
            nombre = $scope.profile.nombre;
        }

        if (!$scope.profile.apellidos) {
            apellidos = "default"; //DNI GENERADO
        }else{
            apellidos = $scope.profile.apellidos;
        }

        if (!$scope.profile.date_birthday) {
            date_birthday = "00-00-0000"; //DNI GENERADO
        }else{
            date_birthday = $scope.profile.date_birthday;
        }

        if (!$scope.profile.dni) {
            dni = "73750421R"; //DNI GENERADO
        }else{
            dni = $scope.profile.dni;
        }



        var data = {'token':$scope.user.token, 'name': nombre,
        'lastname': apellidos,'dni': dni,
        'date_birthday': date_birthday, 'commnity' : com, 
        'province': prov, 'city': pob};
        //console.log(data);


        services.post('login', 'profile', data).then(function (response) {
                console.log(response);

                if (response.success == "true"){
                        CommonService.toastrm("Perfil actualizado", "success");
                        LoginService.login();
                        window.location.href='#/home';
                }

                $scope.AlertMessage = true;

                if (response.error.dni){
                    $scope.profile.dni_error = response.error.dni;
                }

                if (response.error.name){
                    $scope.profile.nombre_error = response.error.name;
                }

                if (response.error.lastname){
                    $scope.profile.surn_error = response.error.lastname;
                }
        });


    };



    $scope.dropzoneConfig = {
        'options': {
            'url': 'backend/index.php?module=products&function=upload',
            addRemoveLinks: true,
            maxFileSize: 1000,
            dictResponseError: "Ha ocurrido un error en el server",
            acceptedFiles: 'image/*,.jpeg,.jpg,.png,.gif,.JPEG,.JPG,.PNG,.GIF,.rar,application/pdf,.psd'
        },
        'eventHandlers': {
            'sending': function (file, formData, xhr) {},
            'success': function (file, response) {
                $scope.avatar = false;
                console.log(response);
                response = JSON.parse(response);
                console.log(response.data);
                if (response.result) {
                    $(".msg").addClass('msg_ok').removeClass('msg_error').text('Success Upload image!!');
                    $('.msg').animate({'right': '300px'}, 300);
                    
                    $scope.user.avatar = response.data;
                    //$scope.avatar = true;

                } else {
                    $(".msg").addClass('msg_error').removeClass('msg_ok').text(response['error']);
                    $('.msg').animate({'right': '300px'}, 300);
                }
            },
            'removedfile': function (file, serverFileName) {
                if (file.xhr.response) {
                    $('.msg').text('').removeClass('msg_ok');
                    $('.msg').text('').removeClass('msg_error');
                    var data = jQuery.parseJSON(file.xhr.response);
                    services.post("user", "delete_avatar", JSON.stringify({'filename': data}));
                }
            }
    }};
    











});







/*var validate_profile = function () {
		var name = document.getElementById('name').value;
	    var lastname = document.getElementById('lastname').value;
	    var dni = document.getElementById('dni').value;
	    var date_birthday = document.getElementById('dni').value;

	    $(".error").remove();

	    if (document.form_user_profile.name.value.length===0){
	        $("#name").focus().after("<span id='e_name' class='error'>Tiene que escribir el usuario</span>");
	        document.form_user_profile.name.focus();
	        return 0;
	    }
	    
	    if (document.form_user_profile.lastname.value.length===0){
	        $("#lastname").focus().after("<span id='e_lastname' class='error'>Tiene que escribir email</span>");
	        document.form_user_profile.lastname.focus();
	        return 0;
	    }
	    
	    if (document.form_user_profile.dni.value.length===0){
	        $("#dni").focus().after("<span id='e_dni' class='error'>Tiene que poner una contraseña</span>");
	        document.form_user_profile.dni.focus();
	        return 0;
	    }

	   	if (document.form_user_profile.date_birthday.value.length===0){
	        $("#date_birthday").focus().after("<span id='e_date_birthday' class='error'>Tiene que poner una fecha</span>");
	        document.form_user_profile.date_birthday.focus();
	        return 0;
	    }
};

$(document).ready(function () {
	$('#date_birthday').datepicker({
		dateFormat: 'dd-mm-yy', 
		changeMonth: true, 
		changeYear: true, 
		yearRange: '1950:2000',
		onSelect: function(selectedDate) {
		}
	});

	$.ajax({
            type : 'POST',
            data: data,
            url  : "../../login/loginjwt",
                success: function(json) {
                    /*console.log(json);*//*
                    user = JSON.parse(json);
                    /*console.log(user);*//*
                    if (user.nombre){
                    	document.getElementById('name').value=user.nombre;
                    }
                    if (user.apellidos){
                    	document.getElementById('lastname').value=user.apellidos;

                    }
                  	if (user.dni){
                  		document.getElementById('dni').value=user.dni;

                  	}
               		if (user.date_birthday){
               			document.getElementById('date_birthday').value=user.date_birthday;

               		}

                },
        });

	$(document).on('click', "#update_products", function() {
		validate_profile();
		var name = document.getElementById('name').value;
	    var lastname = document.getElementById('lastname').value;
	    var dni = document.getElementById('dni').value;
	    var date_birthday = document.getElementById('date_birthday').value;
	    var commnity = document.getElementById('commnity').value;
	    var province = document.getElementById('province').value;
	    var city = document.getElementById('city').value;

      	var data = {'token':user.token, 'name':name,'lastname':lastname,'dni':dni,'date_birthday':date_birthday,'commnity':commnity,'province':province, 'city':city};

      	//console.log(data);

		$.ajax({
            type : 'POST',
            data: data,
            url  : "../../login/profile",
                success: function(json) {
                    console.log(json);
                    user = JSON.parse(json);
                    /*console.log(user);*//*
                    
                    if (user.success='true'){
                    	toastr.success('Perfil actualizado');
                    	setTimeout(function() { 
                    		$callback = user.redirect;
                        	window.location.href=$callback;
                    	}, 1000);
                    }
                    if (user.error){
                    	$(".error").remove();

                    	if (user.error.name){
						        $("#name").focus().after("<span id='e_name' class='error'>" + user.error.name+ "</span>");
						        document.register_user.name.focus();
					    }

					    if (user.error.lastname){
						        $("#lastname").focus().after("<span id='e_lastname' class='error'>"+ user.error.lastname+"</span>");
						        document.register_user.lastname.focus();
						}
					    
					    if (user.error.dni){
						        $("#dni").focus().after("<span id='e_dni' class='error'>"+ user.error.dni+"</span>");
						        document.register_user.dni.focus();
						}

						if (user.error.date_birthday){
						        $("#date_birthday").focus().after("<span id='e_date_birthday' class='error'>"+ user.error.date_birthday+"</span>");
						        document.register_user.date_birthday.focus();
						}
                    }

                },
        });
	});
});*/