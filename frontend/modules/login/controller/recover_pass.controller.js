eventplanner.controller('recover_passCtrl', function ($scope, services, CommonService, token) {

	console.log(token);
	
	$scope.recover_pass = function () {
        //console.log($scope);
        //console.log($scope.fregister.usuario);
        
        passw1 = $scope.frecover_pass.passw;
        passw2 = $scope.frecover_pass.passw2;

        if (passw1 == passw2) {
		    var data = {"password": passw1, "token":token};
			console.log(data);

            services.post('login', 'recover_passBBDD', data).then(function (response) {
                //console.log(response); 
            	if (response.error == 'Success') {               
                	CommonService.toastrm("Contraseña actualizada", "success");
                	window.location.href='#/login';
            	}
            });
        }else{
        	$scope.AlertMessage = true;
        	$scope.error_rpassw2 = "La contraseña no coincide";
        }
    };

});