eventplanner.controller('menuCtrl', function ($scope, services, CommonService, LoginService) {
    LoginService.login();

    $scope.logout = function () {
        LoginService.logout();
        LoginService.login();
        CommonService.toastrm("Desloggeado", "success");
    };


});

eventplanner.controller('loginCtrl', function ($scope, services, CommonService, social, LoginService, localStorage) {
    $scope.facebook = function () {
        social.facebook();
    };

    $scope.twitter = function () {
        social.twitter();
    };

    $scope.google = function () {
        social.google();
    };

    $scope.login = function () {
        //console.log($scope.flogin.usuario);
        //console.log($scope.flogin.passw);
        usuario = $scope.flogin.usuario;
        password = $scope.flogin.passw;
        //console.log(usuario);
        //console.log(password);
        if ((usuario) && (password)) {
            var user = {'type':'login',"usuario": usuario, "password": password};
            //console.log(user);

            services.post('login', 'registernormal', user).then(function (response) {
                //console.log(response);
                $scope.error_usuario = '';
                $scope.error_passw = '';
                        
                if (response.token){
                    localStorage.set(response.token);
                    LoginService.login();
                    CommonService.toastrm("Login correcto", "success");
                    window.location.href='#/home';
                }    

                if (response.error.usuario){
                    $scope.usuario_error = true;
                    $scope.error_usuario = response.error.usuario;
                }
                
                if (response.error.password){
                    $scope.AlertMessage = true;
                    $scope.error_passw = response.error.password;
                }
            });
        }
    };
});

eventplanner.controller('verifyCtrl', function ($scope, event,  CommonService, token, localStorage, LoginService) {
    console.log(event);

    if (event.error == "Success"){
        CommonService.toastrm("Activado", "success");
        localStorage.set(token);
        LoginService.login();
        window.location.href='#/home';
    }
    else{
        CommonService.toastrm("Error de activacion", "error");
        window.location.href='#/login';
    }
});