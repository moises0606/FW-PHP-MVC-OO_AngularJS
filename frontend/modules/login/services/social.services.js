eventplanner.factory("social", ['$location', '$rootScope', 'services', 'CommonService', 'localStorage', 'LoginService',
function ($location, $rootScope, services, CommonService, localStorage, LoginService) {
        var config = {
            apiKey: "XXXXXXXXXXXXXXXXXXXXX",
            authDomain: "fwphpmvcooangularjs.firebaseapp.com",
            databaseURL: "https://fwphpmvcooangularjs.firebaseio.com",
            projectId: "fwphpmvcooangularjs",
            storageBucket: "fwphpmvcooangularjs.appspot.com",
            messagingSenderId: "284662302735"
            };
        firebase.initializeApp(config);

        var service = {};
        service.facebook = facebook;
        service.twitter = twitter;
        service.google = google;
        return service;
        


        function login ($login) {
            const promise = new Promise(function(resolve, reject){
                console.log(firebase.auth());
                var authService = firebase.auth();

                if ($login == "facebook")
                    var provider = new firebase.auth.FacebookAuthProvider();
                if ($login == "twitter")
                    var provider = new firebase.auth.TwitterAuthProvider();
                if ($login == "google")
                    var provider = new firebase.auth.GoogleAuthProvider();

                authService.signInWithPopup(provider)
                .then(function(result) {
                    resolve(result);
                })
                .catch(function(error) {
                    console.log('Detectado un error:', error);
                });
            });
            return promise;
        };

        function save (data) {
            services.post('login', 'RRSS', data).then(function (response) {
                //console.log(response);
                //console.log(response.token);
                localStorage.set(response.token);
                LoginService.login();
                CommonService.toastrm("Login correcto", "success");
                window.location.href='#/home';

            });

        }

        function facebook() {
            login('facebook').then(function(result){
                var email, name, lastname, avatar, uid, user;
                //console.log(result);

                user = CommonService.eliminarDiacriticos(result.additionalUserInfo.profile.first_name);
                email = result.additionalUserInfo.profile.email;
                name = CommonService.eliminarDiacriticos(result.additionalUserInfo.profile.first_name);
                lastname = CommonService.eliminarDiacriticos(result.additionalUserInfo.profile.last_name);
                avatar = result.user.photoURL;
                uid = result.additionalUserInfo.profile.id;

                data = {name:name, lastname:lastname, email:email, avatar:avatar, uid:uid, user:user};
                //console.log(data);

                save(data);
            });
        }

        function twitter() {
            login('twitter').then(function(result){
                var email, name, lastname, avatar, uid, user;
                //console.log(result);

                user = CommonService.eliminarDiacriticos(result.additionalUserInfo.username);
                email = result.additionalUserInfo.profile.email;
                name = CommonService.eliminarDiacriticos(result.additionalUserInfo.profile.name);
                lastname = CommonService.eliminarDiacriticos(result.additionalUserInfo.profile.name);
                avatar = result.user.photoURL;
                uid = result.additionalUserInfo.profile.id_str;

                data = {name:name, lastname:lastname, email:email, avatar:avatar, uid:uid, user:user};
                //console.log(data);
                
                save(data);
            });
        }

        function google() {
            login('google').then(function(result){
                var email, name, lastname, avatar, uid, user;
                //console.log(result);

                user = CommonService.eliminarDiacriticos(result.user.displayName);
                email = result.additionalUserInfo.profile.email;
                name = CommonService.eliminarDiacriticos(result.additionalUserInfo.profile.given_name);
                lastname = CommonService.eliminarDiacriticos(result.additionalUserInfo.profile.family_name);
                avatar = result.user.photoURL;
                uid = result.additionalUserInfo.profile.id;

                data = {name:name, lastname:lastname, email:email, avatar:avatar, uid:uid, user:user};
                //console.log(data);
                
                save(data);
            });
        }
}]);
