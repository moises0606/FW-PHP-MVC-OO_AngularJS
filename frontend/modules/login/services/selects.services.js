eventplanner.factory("com_prov_city", ['services', '$q',
function (services, $q) {
    var service = {};
    service.load_com = load_com;
    service.loadProvincia = loadProvincia;
    service.loadCity = loadCity;
    return service;

    function load_com() {
        var deferred = $q.defer();
        services.get("products", "load_communities", true).then(function (data) {
            //console.log(data);
            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_pais" });
            } else {
                deferred.resolve({ success: true, datas: data });
            }
        });
        return deferred.promise;
    };
    
    function loadProvincia(comunidad) {
        var deferred = $q.defer();
        //console.log(comunidad);
        data = {community : comunidad};
        //console.log(data);
        services.post("products", "load_provinces", data).then(function (data) {
            //console.log(data);
            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_provincias" });
            } else {
                deferred.resolve({ success: true, datas: data });
            }
        });
        return deferred.promise;
    };
    
    function loadCity(provincia) {
        var deferred = $q.defer();
        //console.log(provincia);
        var data = { Provincia : provincia  };
        //console.log(data);
        services.post("products", "load_cities", data).then(function (data) {
            //console.log(data);
            if (data === 'error') {
                deferred.resolve({ success: false, datas: "error_load_poblaciones" });
            } else {
                deferred.resolve({ success: true, datas: data });
            }
        });
        return deferred.promise;
    };
}]);
