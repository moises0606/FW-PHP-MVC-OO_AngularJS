eventplanner.factory("LoginService", ['$location', '$rootScope', 'services', 'localStorage',
function ($location, $rootScope, services, localStorage) {
        var service = {};
        service.login = login;
        service.logout = logout;
        return service;

        function login() {
            localStorage.get().then(function (user){
                //console.log(user);
                if (user.activado == 1){
                    $rootScope.mhome = true;
                    $rootScope.mshop = true;
                    $rootScope.mproducts = false;
                    $rootScope.mcontact = true;
                    $rootScope.mlogin = false;
                    $rootScope.avatar = user.avatar;
                    $rootScope.mprofile = true;
                    $rootScope.mlogout = true;
                    
                    if (user.tipo == 'admin') 
                        $rootScope.mproducts = true;

                }else{

                    if (user.activado == 0)
                        CommonService.toastrm("Usuario desactivado", "error");

                    $rootScope.mhome = true;
                    $rootScope.mshop = true;
                    $rootScope.mproducts = false;
                    $rootScope.mcontact = true;
                    $rootScope.mlogin = true;
                    $rootScope.mprofile = false;
                    $rootScope.mlogout = false;
                }
            });

        }

        function logout() {
            localStorage.remove();
        }
}]);
