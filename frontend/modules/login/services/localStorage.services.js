eventplanner.factory("localStorage", ['services', 'CommonService', '$q', 
function (services, CommonService, $q) { 
        var service = {};
        service.set = set;
        service.get = get;
        service.remove = remove;
        return service;

        function set($token) {
            //console.log($token);
            localStorage.ltoken = JSON.stringify($token);
        }

        function get() {
            var defered=$q.defer();
            var promise=defered.promise;

            if ((localStorage.ltoken) && (localStorage.ltoken!="undefined")){
                token = JSON.parse(localStorage.ltoken);
                //console.log(token);
                data = {token:token};
                //console.log(data.token);

                services.post('login', 'loginjwt', data).then(function (response) {
                    //console.log(response);
                    defered.resolve(response);
                    return response;
                });
            return promise;
            }else
                defered.resolve(false);
                return promise;
        }

        function remove() {
            var ltoken;
            if (window.localStorage){
                //console.log(ltoken);
                localStorage.ltoken = JSON.stringify(ltoken);
            }
        }

}]);
