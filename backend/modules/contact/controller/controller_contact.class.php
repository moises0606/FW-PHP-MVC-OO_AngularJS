<?php
session_start();

class controller_contact {

    function __construct() {
        //include(UTILS . "common.inc.php");
    }

    function send_mailgun(){
      $email=$_POST['mail'];

      $config = array();
      $config['api_key'] = "key-XXXXXXXXXXXXXXXXXXX"; //API Key
      $config['api_url'] = "https://api.mailgun.net/v3/sandboxfd3150a3f1bf4e10afe2929cb89346f9.mailgun.org/messages"; //API Base URL

      $message = array();
      $message['from'] = "murianamoises@gmail.com";
      $message['to'] = $email;
      $message['h:Reply-To'] = "murianamoises@gmail.com";
      $message['subject'] = "Supportinfo";
      $message['html'] = 'Hello ' . $email . ',</br></br> Thanks for contact with us';
     
      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $config['api_url']);
      curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
      curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
      curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
      curl_setopt($ch, CURLOPT_POST, true); 
      curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
      $result = curl_exec($ch);
      curl_close($ch);

      if ($result){
        echo json_encode('true');
      }else {
        echo json_encode('false');
      }
    }
}