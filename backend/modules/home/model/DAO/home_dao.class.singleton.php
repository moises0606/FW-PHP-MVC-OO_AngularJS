<?php

class home_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function obtain_entries_DAO($db){
        $sql = "SELECT prodname, prodref FROM productos";

        return $db->listar($sql);
    }


    public function obtain_entries2_DAO($db, $arrArgument){
        $key = $arrArgument['key'];

        $sql = "SELECT prodname,prodprice,province,prodref,city,latitud,longitud,prodpic,date_reception,date_expiration,proddesc FROM productos WHERE province LIKE '%$key%'";

        return $db->listar($sql);
    }

    public function page_products_DAO($db) {
        $sql = "SELECT prodname,prodprice,province,prodref,city,latitud,longitud,prodpic,date_reception,date_expiration,proddesc FROM productos";

        return $db->listar($sql);
    }

    public function obtain_provinces_DAO($db){
        $sql = "SELECT slug FROM provincias ORDER BY population DESC";

        return $db->listar($sql);
    }

    public function obtain_provinces_autocomplete_DAO($db){
          $sql = "SELECT slug FROM provincias";

          return $db->listar($sql);
    }

    public function obtain_pages_DAO($db, $key){
        $sql = "SELECT COUNT(*) as total FROM productos WHERE province LIKE '%$key%' OR prodname LIKE '%$key%'";

        return $db->listar($sql);
    }

    public function obtain_comments_DAO($db){
        $sql = "SELECT * FROM comentarios";

        return $db->listar($sql);
    }

    public function insert_comment_DAO($db, $data){
        $prodref = $data['prodref'];
        $comentario = $data['comment']; 

        $sql = "INSERT INTO `entradas_v2`.`comentarios` (`prodref`,`comment`) VALUES ('$prodref','$comentario');";

        return $db->ejecutar($sql);
    }

    public function insert_rating_DAO($db, $data){
        $prodref = $data['prodref'];
        $usuario = $data['user']; 
        $puntuacion = $data['puntuacion'];

        $sql = "INSERT INTO `entradas_v2`.`rating`(`user`,`entrada`,`rating`)VALUES('$usuario','$prodref','$puntuacion');";

        return $db->ejecutar($sql);
    }

    public function obtain_rating_DAO($db){
        $sql = "SELECT count(*) AS opiniones, round(avg(rating),2) AS puntuacion, entrada FROM rating GROUP BY entrada";

        return $db->listar($sql);
    }
}