<?php
session_start();

class controller_home {

    function __construct() {
        //include(UTILS . "common.inc.php");
    }

    function entry(){
        $json = array();
        $json2 = array();
        $data = array();

        try{
        $json = loadModel(MODEL_MAIN, "home_model", "obtain_entries");
        $json2 = loadModel(MODEL_MAIN, "home_model", "obtain_provinces_autocomplete");

        $data = array_merge($json, $json2);

        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($data);
        exit;
    }

    function category(){
        $json = array();

        try{
        $json = loadModel(MODEL_MAIN, "home_model", "obtain_provinces");

        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($json);
        exit;
    }

    function list1(){
        $json = array();
        
            try{
                $json = loadModel(MODEL_MAIN, "home_model", "page_products");
            }catch (Exception $e){
                echo json_encode("error");
                exit;
            }
            echo json_encode($json);
            exit;
    }

    function list_results(){
        $json = array();

        $arrArgument = array(
                'key' => $_GET['param']
            );
        try{
            $json = loadModel(MODEL_MAIN, "home_model", "obtain_entries2", $arrArgument);
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($json);
        exit;
    }

    function savecat(){
        $_SESSION['clave']=$_POST['clave'];
        if (isset($_SESSION['clave'])){
            echo json_encode('true');
        }else{
            echo json_encode('false');
        }
        exit;
    }

    function obtain_comentarios(){
       $json = array();

        try{
            $json = loadModel(MODEL_MAIN, "home_model", "obtain_comments");
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($json);
        exit;
    }

    function insert_comment(){
       $json = array();
       print_r($_POST);

        try{
            $json = loadModel(MODEL_MAIN, "home_model", "insert_comment", $_POST);
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($json);
        exit;
    }

    function insert_rating(){
       $json = array();

        try{
            $json = loadModel(MODEL_MAIN, "home_model", "insert_rating", $_POST);
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($json);
        exit;
    }

    function obtain_rating(){
       $json = array();

        try{
            $json = loadModel(MODEL_MAIN, "home_model", "obtain_rating");
        }catch (Exception $e){
            echo json_encode("error");
            exit;
        }
        echo json_encode($json);
        exit;
    }
}