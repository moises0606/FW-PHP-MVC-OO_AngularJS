<?php

function validate_user($value){
    $error = array();
    $valid = true;
    $filtro = array(
        'usuario' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp'=>'/^\D{3,20}$/')
        ),
        'email' => array(
                'filter'=>FILTER_CALLBACK,
                'options'=>'validatemail'
        ),

    );
        $resultado=filter_var_array($value,$filtro);
        $resultado['password'] = $value['password'];

        if(!$resultado['usuario']){
            $error['usuario']='Usuario debe tener de 3 a 20 caracteres';
        }elseif(!$resultado['email']){
            $error['email']='El email debe ser valido';
        }else{
             return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
        };
        return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
}//End of function validate user

function validate_email_recover($value){
    $error = array();
    $valid = true;
    $filtro = array(
        'email' => array(
                'filter'=>FILTER_CALLBACK,
                'options'=>'validatemail'
        ),

    );
        $resultado=filter_var_array($value,$filtro);
        $resultado['password'] = $value['password'];

        if(!$resultado['email']){
            $error['email']='El email debe ser valido';
        }else{
             return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
        };
        return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
}//End of function validate email recover

function validate_profile($value){
    $error = array();
    $valid = true;
    $filtro = array(
        'name' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp'=>'/^\D{3,20}$/')
        ),
        'lastname' => array(
            'filter' => FILTER_VALIDATE_REGEXP,
            'options' => array('regexp'=>'/^\D{3,20}$/')
        ),
        'dni' => array(
            'filter'=>FILTER_CALLBACK,
            'options'=>'validar_dni'
        ),
    );
        $resultado=filter_var_array($value,$filtro);
        $resultado['commnity'] = $value['commnity'];
        $resultado['province'] = $value['province'];
        $resultado['city'] = $value['city'];
        $resultado['date_birthday'] = $value['date_birthday'];
        $resultado['token'] = $value['token'];

        if(!$resultado['name']){
            $error['name']='El nombre debe tener de 3 a 20 caracteres';
        }elseif(!$resultado['lastname']){
            $error['lastname']='El apellido debe tener de 3 a 20 caracteres';
        }elseif(!$resultado['dni']){
            $error['dni']='El dni debe ser valido';
        }else{
             return $return=array('resultado'=>true,'error'=>$error,'datos'=>$resultado);
        };
        return $return=array('resultado'=>false , 'error'=>$error,'datos'=>$resultado);
}//End of function validate profile

/*-----------------------------------------------------------------------*/
function validatemail($email){
        $email = filter_var($email, FILTER_SANITIZE_EMAIL);
        if(filter_var($email, FILTER_VALIDATE_EMAIL)){
            if(filter_var($email, FILTER_VALIDATE_REGEXP, array('options' => array('regexp'=> '/^.{5,40}$/')))){
                return $email;
            }
        }
        return false;
}

function send_mail_activation($email, $token) {
    $config = array();
    $config['api_key'] = "key-XXXXXXXXXXXXXXXXXXXXXXX"; //API Key
    $config['api_url'] = "https://api.mailgun.net/v3/sandboxfd3150a3f1bf4e10afe2929cb89346f9.mailgun.org/messages"; //API Base URL

    $message = array();
    $message['from'] = "murianamoises@gmail.com";
    $message['to'] = $email;
    $message['h:Reply-To'] = "murianamoises@gmail.com";
    $message['subject'] = "Supportinfo";
    $message['html'] = 'Click on following link to activate
    http://127.0.0.1/programacio/workspace_moises/FW-PHP-MVC-OO_AngularJS/#/register/activate/'.$token;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $config['api_url']);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_POST, true); 
    curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function send_mail_recover_pass($email, $token) {
    $config = array();
    $config['api_key'] = "key-02a584cabbe35f25a7ad6e06c6244d96"; //API Key
    $config['api_url'] = "https://api.mailgun.net/v3/sandboxfd3150a3f1bf4e10afe2929cb89346f9.mailgun.org/messages"; //API Base URL

    $message = array();
    $message['from'] = "murianamoises@gmail.com";
    $message['to'] = $email;
    $message['h:Reply-To'] = "murianamoises@gmail.com";
    $message['subject'] = "Supportinfo";
    $message['html'] = 'Click on following link to activate
    http://127.0.0.1/programacio/workspace_moises/FW-PHP-MVC-OO_AngularJS/#/recover_pass/activate/'.$token;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $config['api_url']);
    curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
    curl_setopt($ch, CURLOPT_USERPWD, "api:{$config['api_key']}");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_POST, true); 
    curl_setopt($ch, CURLOPT_POSTFIELDS,$message);
    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
}

function get_gravatar( $email, $s = 80, $d = 'wavatar', $r = 'g', $img = false, $atts = array() ){
    $email = trim($email);
    $email = strtolower($email);
    $email_hash = md5($email);
    
    $url = "https://www.gravatar.com/avatar/".$email_hash;
    $url .= md5( strtolower( trim( $email ) ) );
    $url .= "?s=$s&d=$d&r=$r";

    if ( $img ) {
        $url = '<img src="' . $url . '"';
        foreach ( $atts as $key => $val )
            $url .= ' ' . $key . '="' . $val . '"';
        $url .= ' />';
    }
    return $url;
}

function validar_dni($dni){
    $letra = substr($dni, -1);
    $numeros = substr($dni, 0, -1);
        if ( substr("TRWAGMYFPDXBNJZSQVHLCKE", $numeros%23, 1) == $letra && strlen($letra) == 1 && strlen ($numeros) == 8 ){
            return $dni;
        }else{
            return false;
        } 
}