<?php

class login_dao {
    static $_instance;

    private function __construct() {

    }

    public static function getInstance() {
        if(!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function isthereRRSS_DAO($db, $uid){
        $sql = "SELECT count(*) AS isthere FROM usuarios WHERE uid='$uid' AND activado=1";

        return $db->listar($sql);
    }

    public function inputRRSS_DAO($db, $user){
        $user1 = $user['user'];
        $email = $user['email'];
        $name = $user['name'];
        $lastname = $user['lastname'];
        $avatar = $user['avatar'];
        $uid = $user['uid'];
        $token = $user['token'];

        $sql = "INSERT INTO `entradas_v2`.`usuarios`(`usuario`,`email`,`nombre`,`apellidos`,`avatar`,`tipo`,`uid`,`activado`,`token`) VALUES ('$user1','$email','$name','$lastname','$avatar','user','$uid',1,'$token')";

        return $db->ejecutar($sql);
    }

    public function loginRRSS_DAO($db, $uid1){
        $uid=$uid1['uid'];
        $token=$uid1['token'];

        $sql = "UPDATE usuarios SET token='$token' WHERE uid='$uid'";

        return $db->ejecutar($sql);
    }

    public function loginjwt_DAO($db, $token){
        $sql = "SELECT * FROM usuarios WHERE token='$token'";

        return $db->listar($sql);
    }

    public function register_DAO($db, $user){
        $user1 = $user['usuario'];
        $email = $user['email'];
        $avatar = $user['avatar'];
        $uid = $user['usuario'];
        $token = $user['token'];
        $password = $user['password'];

        $sql = "INSERT INTO `entradas_v2`.`usuarios`(`usuario`,`email`,`avatar`,`tipo`,`uid`,`activado`,`token`,`password`) VALUES ('$user1','$email','$avatar','user','$uid',0,'$token','$password')";

        return $db->ejecutar($sql);
    }
    
    public function activate_DAO($db, $token){
        $sql = "UPDATE usuarios set activado=1 WHERE token='$token'";

        return $db->ejecutar($sql);
    }

    public function loginnormal_DAO($db, $user){
        $uid=$user['usuario'];
        $password=$user['password'];
        $token=$user['token'];

        $sql = "UPDATE usuarios SET token='$token' WHERE uid='$uid' AND password='$password'";

        return $db->ejecutar($sql);
    }

    public function update_profile_DAO($db, $user){
        $name=$user['name'];
        $lastname=$user['lastname'];
        $dni=$user['dni'];
        $date_birthday=$user['date_birthday'];
        $commnity=$user['commnity'];
        $province=$user['province'];
        $city=$user['city'];
        $avatar=$user['avatar'];
        $token=$user['token'];

       

        $sql = "UPDATE usuarios SET nombre='$name', apellidos='$lastname', dni='$dni', date_birthday='$date_birthday', comunidad='$commnity', provincia='$province', poblacion='$city', avatar='$avatar' WHERE token='$token'";

        return $db->ejecutar($sql);
    }

    public function recover_email_DAO($db, $user){
        $email=$user['email'];
        $token=$user['token'];

        $sql = "UPDATE usuarios SET token='$token', activado=0 WHERE email='$email'";

        return $db->ejecutar($sql);
    }

    public function isthere_email_DAO($db, $email){
        $sql = "SELECT count(*) AS isthere FROM usuarios WHERE email='$email'";

        return $db->listar($sql);
    }

    public function recover_pass_DAO($db, $user){
        $password=$user['password'];
        $token=$user['token'];

        $sql = "UPDATE usuarios SET password='$password', activado=1 WHERE token='$token'";

        return $db->ejecutar($sql);
    }

    public function obtain_rating_DAO($db){
        $sql = "SELECT * FROM rating";

        return $db->listar($sql);
    }
}

