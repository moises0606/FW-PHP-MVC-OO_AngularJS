<?php

class login_bll{
    private $dao;
    private $db;
    static $_instance;

    private function __construct() {
        $this->dao = login_dao::getInstance();
        $this->db = Db::getInstance();
    }

    public static function getInstance() {
        if (!(self::$_instance instanceof self)){
            self::$_instance = new self();
        }
        return self::$_instance;
    }

    public function isthereRRSS_BLL($token){
      return $this->dao->isthereRRSS_DAO($this->db, $token);
    }

    public function inputRRSS_BLL($user){
      return $this->dao->inputRRSS_DAO($this->db, $user);
    }

    public function loginRRSS_BLL($user){
      return $this->dao->loginRRSS_DAO($this->db, $user);
    }

    public function loginjwt_BLL($token){
      return $this->dao->loginjwt_DAO($this->db, $token);
    }

    public function register_BLL($user){
      return $this->dao->register_DAO($this->db, $user);
    }

    public function activate_BLL($token){
      return $this->dao->activate_DAO($this->db, $token);
    }

    public function loginnormal_BLL($user){
      return $this->dao->loginnormal_DAO($this->db, $user);
    }

    public function update_profile_BLL($user){
      return $this->dao->update_profile_DAO($this->db, $user);
    }

    public function recover_email_BLL($user){
      return $this->dao->recover_email_DAO($this->db, $user);
    }

    public function isthere_email_BLL($email){
      return $this->dao->isthere_email_DAO($this->db, $email);
    }

    public function recover_pass_BLL($user){
      return $this->dao->recover_pass_DAO($this->db, $user);
    }

    public function obtain_rating_BLL(){
      return $this->dao->obtain_rating_DAO($this->db);
    }
}