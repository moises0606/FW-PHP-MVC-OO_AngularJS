-- MySQL dump 10.13  Distrib 5.7.21, for Linux (x86_64)
--
-- Host: localhost    Database: entradas_v2
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.13-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `usuarios`
--

DROP TABLE IF EXISTS `usuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `usuarios` (
  `uid` varchar(200) COLLATE latin1_spanish_ci NOT NULL,
  `usuario` varchar(100) CHARACTER SET latin1 NOT NULL,
  `email` varchar(100) CHARACTER SET latin1 NOT NULL,
  `nombre` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `apellidos` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `dni` varchar(9) CHARACTER SET latin1 DEFAULT NULL,
  `password` varchar(200) CHARACTER SET latin1 NOT NULL,
  `date_birthday` varchar(45) CHARACTER SET latin1 DEFAULT NULL,
  `comunidad` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `provincia` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `poblacion` varchar(100) CHARACTER SET latin1 DEFAULT NULL,
  `avatar` varchar(200) CHARACTER SET latin1 DEFAULT NULL,
  `tipo` varchar(45) CHARACTER SET latin1 NOT NULL,
  `token` varchar(1000) CHARACTER SET latin1 NOT NULL,
  `activado` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_spanish_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `usuarios`
--

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;
INSERT INTO `usuarios` VALUES ('115512667412353748872','Moises Muriana Goya','murianamoises@gmail.com','Moises','Muriana Goya',NULL,'',NULL,NULL,NULL,NULL,'https://lh3.googleusercontent.com/-XdUIqdMkCWA/AAAAAAAAAAI/AAAAAAAAAAA/4252rscbv5M/photo.jpg','user','eb229e00cd013eefdc04800125b567ce2ebb07f46a7b37624e46fbb0b1bf4de0',1),('1577545979039067','Moises','murianamoises@hotmail.com','Moises','Muriana Goya',NULL,'',NULL,NULL,NULL,NULL,'https://graph.facebook.com/1577545979039067/picture','user','5b80639e3c29402ffe76bc0b5e34cc2fab528eb487771880f9783cfd490c311b',1),('802120387','MurianaGoya','murianamoises@gmail.com','Moises Muriana Goya','Moises Muriana Goya',NULL,'',NULL,NULL,NULL,NULL,'https://abs.twimg.com/sticky/default_profile_images/default_profile_normal.png','user','3e05c41f85aebb4125b8224fc02ab9369f4603bb8682700f34e3dfc6eb33baf5',1),('moises','moises','murianamoises@gmail.com','sssss','sss','48606014E','e32fbdaaaec852fbf73d42aff53d320ae8dc9403ce3283a064b8e00182119faf','15-05-2018','andalucia','almeria','abla','https://www.gravatar.com/avatar/8a9eb26da73bad0fc2b423b923d11c858a9eb26da73bad0fc2b423b923d11c85?s=80&d=identicon&r=g','user','f1c732fc8438cb5a50a0d8d194ddbb105d380080c4dfde5720d3ec8ec4a6e69b',1);
/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-15 16:38:55
