INTERFAZ--------------------------
Cuando accedemos a la Aplicacion Web, podremos observar que en la parte de arriba
esta el menu, desde aquí podremos acceder a los diferentes modulos dependiendo de que tipo
de usuario seamos (admin, user, guest)
Si somos guest, podremos acceder al HOME, SHOP, CONTACT, LOGIN
Si somos user, podremos acceder a lo mismo que el usuario guest pero podremos acceder al PROFILE y podremos hacer LOGOUT


A la parte de bajo de la pagina, veremos el FOOTER, tenemos uno fijo (siempre visible) y otro que lo 
podremos ver si bajamos hacia bajo en la pagina.
----------------------------------

------------------------------HOME
En el home podemos ver unas categorias ordenadas por popularidad, en el boton de load_more podremos solicitar que angular js nos muestre 4 categorias mas.
Cuando pulsamos en una categoria, nos redirige al shop, enviando como clave esta categoria
El autocomplete, nos muestra categorias y entradas, cuando hacemos click en search, nos redirige al shop, enviando como clave lo que hemos escrito
----------------------------------

---------------------- SHOP
Cuando entramos directos pulsando el boton de shop:
	En shop un input para filtrar las entradas mostradas. 
	Todos los datos son solicitados a la BBDD, desde controller JS traves de services al controlador php i solicitados desde
	el DAO, devueltos con json_encode.
	Cuando tenemos la informacon el controllador js le enviara las entradas que queramos mostrar a angular.
	A traves de un servicio pintamos el maps
	Cuando hacemos clic en una entrada angular js nos mostrara los detalles de la entrada seleccionada.

Si llegamos a shop desde el home, con una categoria o filtro, se solicitará al servidor todos los datos que se relacionen con este filtro.

Las entradas estarán paginadas con la paginacion de angular js
----------------------------------

-----------------------------LOGIN
LOGIN
	Desde el login podemos loggearnos, introducimos el usuario, y contraseña, angular le pasará el usuario y contraseña
	al controlador js y lo enviaremos a traves de services al backend, la funcion de php en el controlador php generamos un token que le pasará la informacion al DAO utilizando el model y el bll, para comprobar si el usuario existe y si la contraseña es correcta, guardará el token en BBDD, la respuesta con el token es devuelta al controlador php y enviada a angular JS a traves de json_encode.
	Si el usuario no existe JS pintará el error y en el caso que la constraseña sea incorrecta, mostrará el error.
	Si se ha realizado con exito se guardara el token en localStorage, mostrará un toastr y redireccionara al Home.

LOGIN SOCIAL
	Podremos loggearnos clicando en la red social que deseamos, introduciremos nuestros datos, el controller js (utilizando factoria de servicios) enviara los datos a traves de services al controlador php y este comprobara si el usuario existe en BBDD, si no existe lo registrara, devolvera el token generado y guardado en BBDD, para posteriormente guardarlo en localStorage

REGISTER
	Desde el register nos podremos registrar, siempre se realizará como usuario normal (user), controller JS le pasará la informacion al 
	controlador php a traves de service y este le pasará la informacion al DAO utilizando el model y el bll, para introducir el usuario la respuesta es devuelta al controlador php y se pasa al controllador JS a través de json_encode. Si el usuario o email ya existen, angular JS mostrará el error.
	Si se ha realizado con exito, mostrará un toastr y enviará un correo con el token al usuario.

	Cuando pinxa en el link, se envia al controllador js de verify y verifica el usuario poniendo 1 en activated, y a la vez se guarda el token en localStorage para logearse automaticamente, redirecciona al HOME

RECOVER_PASSWORD
	El email se envia a traves de services al controlador php y este verifica con una consulta a la BBDD a traves del DAO utilizando el model y el bll, 
	la informacion es devuelta al controlador php y enviada al controller JS a traves de json_encode, mientras que angular JS esta pintando si el email no existe, si existe muestra toastr y redirige al home y enviara un correo con el token al usuario y pondra el usuario a 0 en activated. 
	
	Cuando pinxa en el link, se envia al recover_pass, el usuario introducira la nueva contraseña y repetirla, si no coincide, muestra el error, si coincide envia el email, la contraseña y el token a traves de services al controlador php y actualiza la contraseña con una consulta
	desde el DAO utilizando el model y el bll, el controlador php devolvera si se ha realizado con exito a traves de json_encode, si ha sido un exito, mostrará un toastr y redireccionara al Login.

LOGOUT
	Desde el controlador js, destruiremos el token del usuario.
----------------------------------

---------------------------CONTACT
Desde este modulo el usuario tendra toda la informacion disponible para poder contactar con nosotros, en este caso
le mostramos Lorem Ipsum
----------------------------------

-----------------------------INDEX
Desde el Index tenemos cargadas todas las librerias, directivas y controladores js
----------------------------------

---------------------------FOOTER
Desde el Footer podremos ver informacion sobre la empresa que en este caso es Lorem Ipsum y si el usuario desea mas información al pulsar Read More lo redirigiremos a Contact por otra parte también tenemos los botones para llevarnos a las redes sociales en este caso los botones no tienen ninguna accion asociada.
También podremos encontrar la direccion de nuestra sede y si queremos mandar una consulta, nos redigira a Contact
----------------------------------